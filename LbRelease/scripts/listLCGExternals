#!/usr/bin/env python
###############################################################################
# (c) Copyright 2016- CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Script to generate the LCG tarball based on the RPMs provided by PH-SFT

Created on Oct 18, 2016

@author: Ben Couturier
'''

from LbUtils.Script import Script
from LbRelease import LCGYumInstaller

import logging
import re
import sys
import os

class ListExternals(Script):

    def defineOpts(self):
        parser = self.parser
        parser.set_defaults(output_dir=None)
        parser.add_option("-s","--siteroot",
                          help = "temporary directory where the RPMs will be installed before repackaging"
                          "[default: %default]",
                          default="/tmp/siteroot")


    def main(self):
        log = logging.getLogger()
        self.log = log
        args = self.args
        opts = self.options
        version = None

        if len(args) < 2:
            self.parser.error("Not enough arguments, please specify CMTCONFIG externals_filename")
            sys.exit(1)
        else:
            cmtconfig = args[0]
            filename = args[1]

        self.filename = filename
        self.cmtconfig = cmtconfig

        # Setting the CMTCONFIG to the requested one
        os.environ['CMTCONFIG'] = self.cmtconfig

        # preparing the actual RPM install area
        installer = LCGYumInstaller.LCGYumInstaller(opts.siteroot)
        self._installer = installer

        # Retrieving the list of externals from the mentioned project
        (lcgVer, externalsList) = self.getExternalsList(filename)
                
        # Getting the list of packages for LCG
        log.warning("Loading list of LCG packages from YUM")
        alllcg = list(installer.list("LCG_%s_" % lcgVer))
        
        log.warning("Matching list of packages with LCG list")
        packDict = {}
        for ext in externalsList:
            packDict[ext] = None
            extName = "^LCG_%s_%s_.*_%s.*$" % (lcgVer, ext, cmtconfig.replace("-", "_"))
            for p in alllcg:
                if re.match(extName, p.rpmName()):
                    packDict[ext] = p
                    log.debug("%s -> %s" % ( ext, p.rpmName()))

        for key, package in packDict.iteritems():
            if package is not None:
                print package.rpmName()
            

    def getExternalsList(self, filename):
        '''
        Gets the list of all externals needed (as from LCG 86)
        '''
        self.log.warning("Loading list of externals from file")
        # Cache the externals dictionary for the version...
        # Loading the metadata
        import json
        with open(filename) as f:
            data = json.load(f)

        # Looking for LCG version
        # We need a file like so:
        # {
        #  "heptools": {
        #    "version": 84
        #    "packages":[
        #      "AIDA",
        #      "Boost",
        #      "CLHEP",
        #      "COOL"
        #    ]
        #  }
        # }

        heptools = data["heptools"]
        self.lcgver = heptools["version"]
        self.externalsList = heptools["packages"]
        return (self.lcgver, self.externalsList)

if __name__ == '__main__':
    s = ListExternals(usage="%prog [options] cmtconfig json_filename")
    sys.exit(s.run())

