'''

Tools to manage the lb-run configuration


Created on March 3, 2016
@author: Ben Couturier
'''
import json
import logging
import os
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

from LbConfiguration.Platform import getHostOs


def loadConfig( dir = None, fname = 'projectConfig.json' ):
    '''
    Loads the project configuration from the JSON file
    '''
    if dir != None:
        fname = os.path.join(dir, fname)

    res = None
    with open(fname) as f:
        res = json.load(f)

    return res

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    indent(elem)
    return '<?xml version="1.0" encoding="UTF-8"?>\n' \
        + ET.tostring(elem, encoding='utf-8')


def getLCGVersion( lcgVersion, lcgBinary, package ):
    from LbConfiguration.SP2.lookup import findLCG
    from LbConfiguration.SP2.version import LCGInfoName
    fileInfo = os.path.join( findLCG( lcgVersion, lcgBinary ), LCGInfoName( lcgBinary ) )
    package = package.lower()  # caseinsensitive lookup
    need_compiler = package == 'gcc'
    for l in open( fileInfo ):
        if need_compiler and l.startswith( 'COMPILER:' ):
            return l.split( ';' )[1].strip()
        l = l.split( ';' )
        if l[0].strip().lower() == package:
            return l[2].strip()
    return None

def getLCGPath( lcgVersion, lcgBinary, package ):
    from LbConfiguration.SP2.lookup import findLCG
    from LbConfiguration.SP2.version import LCGInfoName
    fileInfo = os.path.join( findLCG( lcgVersion, lcgBinary ), LCGInfoName( lcgBinary ) )
    package = package.lower()  # caseinsensitive lookup
    special_markers = ["PLATFORM:",
                       "VERSION:",
                       "COMPILER:"]

    for l in open( fileInfo ):
        for m in special_markers:
            if l.strip().startswith(m):
                continue
        l = l.split( ';' )
        if l[0].strip().lower() == package:
            return l[3].strip()
    return None


class ManifestGenerator( object ):
    '''
    Generates a Manifest.xml file like:

    <?xml version="1.0" encoding="UTF-8"?>
    <manifest>
      <project name="LbScripts" version="HEAD" />
      <heptools>
        <version>83</version>
        <packages>
          <package name="Python" version="2.7.9.p1" />
          <package name="pytools" version="1.9_python2.7" />
        </packages>
       </heptools>
       <used_projects>
         <project name="LHCbGrid" version="v9r3" />
         <project name="Dirac" version="v6r14p24" />
       </used_projects>
    </manifest>

    Given the JSON project configuration.

    '''
    def __init__(self, config):
        self._config = config


    def getDocument(self):
        ''' Builds the XML document based on the config provided '''
        manifest = ET.Element('manifest')
        project = ET.SubElement(manifest, 'project')
        project.set('name', self._config['name'])
        project.set('version', self._config['version'])
        heptoolsConfig = self._config.get('heptools', None)
        if heptoolsConfig != None:
            heptools = ET.SubElement(manifest, 'heptools')
            # Checking the heptools version and setting it
            if heptoolsConfig.get('version') == None:
                raise Exception("Missing version for heptools")
            heptoolsVersion = ET.SubElement(heptools, 'version')
            heptoolsVersion.text = str(heptoolsConfig.get('version'))

            # Checking the heptools binary_tag and setting it
            if heptoolsConfig.get( 'binary_tag' ) == None:
                raise Exception( "Missing binary_tag for heptools" )
            heptoolsBinary = ET.SubElement( heptools, 'binary_tag' )
            heptoolsBinary.text = str( self._config['cmtconfig'] )
            # Now the list of packages
            packagesConfig = heptoolsConfig.get('packages')
            if packagesConfig != None:
                packages =  ET.SubElement(heptools, 'packages')
                for pack_info in packagesConfig:
                    ( p, definev, t ) = pack_info[0:3]
                    v = getLCGVersion( heptoolsVersion.text, heptoolsBinary.text, p ) or definev
                    ET.SubElement( packages, 'package', attrib = { "name":p, "version":v} )

        exttoolsConfig = self._config.get( 'exttools', None )
        if exttoolsConfig != None:
            exttools = ET.SubElement( manifest, 'exttools' )

            if exttoolsConfig.get( 'binary_tag' ) != None:
              exttoolsBinary = ET.SubElement( exttools, 'binary_tag' )
              # As for the HEP config, set the default CMTCONFIG
              exttoolsBinary.text = str(self._config['cmtconfig'])

            # Now the list of packages
            packagesConfig = exttoolsConfig.get( 'packages' )
            if packagesConfig != None:
                packages = ET.SubElement( exttools, 'packages' )
                for pack_info in packagesConfig:
                    ( p, v, t ) = pack_info[0:3]
                    ET.SubElement( packages, 'package', attrib = { "name":p, "version":v} )

        usedProjectConfig = self._config.get( 'used_projects', None )
        if usedProjectConfig != None:
            usedProject = ET.SubElement( manifest, 'used_projects' )
            projectConfig = usedProjectConfig.get( 'project' )
            if projectConfig != None:
            # Now the list of packages
               for ( p, v, t ) in projectConfig:
                   ET.SubElement( usedProject, 'project', attrib = { "name":p, "version":v} )

        return manifest


class XEnvGenerator(object):
    '''
    Generates an xenv file like:

<?xml version="1.0" encoding="UTF-8"?>
<env:config xmlns:env="EnvSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="EnvSchema EnvSchema.xsd ">
  <env:default variable="LCG_releases_base">/afs/cern.ch/sw/lcg/releases</env:default>
  <env:default variable="CMTCONFIG">x86_64-slc6-gcc49-opt</env:default>
  <env:default variable="BINARY_TAG">${CMTCONFIG}</env:default>

  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>

  <env:prepend variable="PATH">${.}/LbConfiguration/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbConfiguration/python</env:prepend>
  <env:set variable="LBCONFIGURATIONROOT">${.}/LbConfiguration</env:set>

  <env:prepend variable="PATH">${.}/LbLegacy/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbLegacy/python</env:prepend>
  <env:set variable="LBLEGACYROOT">${.}/LbLegacy</env:set>

  <env:prepend variable="PATH">${.}/LbNightlyTools/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbNightlyTools/python</env:prepend>
  <env:set variable="LBNIGHTLYTOOLSROOT">${.}/LbNightlyTools</env:set>

  <env:prepend variable="PATH">${.}/LbRelease/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbRelease/python</env:prepend>
  <env:set variable="LBRELEASEROOT">${.}/LbRelease</env:set>

  <env:prepend variable="PATH">${.}/LbScriptsPolicy/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbScriptsPolicy/python</env:prepend>
  <env:set variable="LBSCRIPTSPOLICYROOT">${.}/LbScriptsPolicy</env:set>

  <env:prepend variable="PATH">${.}/LbUtils/scripts</env:prepend>
  <env:prepend variable="PYTHONPATH">${.}/LbUtils/python</env:prepend>
  <env:set variable="LBUTILSROOT">${.}/LbUtils</env:set>

  <env:prepend variable="CMAKE_PREFIX_PATH">${.}/LbRelease/data/DataPkgEnvs:${.}/LbUtils/cmake</env:prepend>

</env:config>

'''
    def __init__(self, config):
        self._config = config


    def getDocument( self ):
        ''' Builds the XML document based on the config provided '''
        import sys
        # Portable way to register namespace across versions of Element tree
        try:
            register_namespace = ET.register_namespace
        except:
            def register_namespace(prefix, uri):
                ET._namespace_map[uri] = prefix

        register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        register_namespace('env', 'EnvSchema')
        config = ET.Element( '{EnvSchema}config' )
        config.set("{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", "EnvSchema EnvSchema.xsd ")

        if self._config.has_key( 'dir_base' ):
          dir_base = self._config['dir_base']
        else:
          dir_base = '.'

        # Adding vars
        def add(tag, var, val):
            d = ET.SubElement(config, tag, attrib={  "variable": var })
            d.text = val
            return d

        def addValue( tag, val ):
            d = ET.SubElement( config, tag )
            d.text = val
            return d

        add( "env:default", "LCG_releases_base", "/afs/cern.ch/sw/lcg/releases" )
        add( "env:default", "LCG_external_area", "/afs/cern.ch/sw/lcg/external" )
        if self._config.has_key( 'cmtconfig' ):
          add( "env:default", "CMTCONFIG", "%s" % self._config['cmtconfig'])
          add( "env:default", "LCG_hostos",
               "%s" % getHostOs(self._config['cmtconfig']))
        else:
          default = "x86_64-slc6-gcc49-opt"
          add( "env:default", "CMTCONFIG", default)
          add( "env:default", "LCG_hostos",
               "%s" % getLCGhostos(default))

        if self._config.has_key( 'python_version' ):
          add( "env:default", "PYTHON_VERSION_TWO", "%s" % self._config['python_version'] )
        else:
          add( "env:default", "PYTHON_VERSION_TWO", "2.7" )

        add( "env:default", "BINARY_TAG", "${CMTCONFIG}" )

        projectConfig = self._config.get( 'used_projects', None )
        def addProject( projectConfig ):
          if projectConfig == None:
            return
          projectProject = projectConfig.get( 'project' )
          platform = self._config.get( 'cmtconfig', "x86_64-slc6-gcc49-opt" ) 
          for p, v, t in projectProject:
            for path, suffix in [( path, suffix )
                                 for path in ( os.environ.get( 'User_release_area', '' ).split( os.pathsep ) +
                                               os.environ.get( 'CMAKE_PREFIX_PATH', '' ).split( os.pathsep ) +
                                               os.environ.get( 'CMTPROJECTPATH', '' ).split( os.pathsep ) )
                                 for suffix in ["{P}/{P}_{v}/InstallArea/{c}".format( P = p.upper(), p = p, v = v,
                                                                                      c = platform ),
                                                "{P}/{P}_{v}".format( P = p.upper(), p = p, v = v ),
                                                "{p}".format(p=p),
                                                "{p}/InstallArea/{c}".format(p=p, c=platform)]]:
              if os.path.exists( os.path.join( path, suffix, "{p}.xenv".format( p = p ) ) ):
                addValue( "env:search_path", os.path.join( path, suffix ) )
                break

            addValue( "env:include", "%s.xenv" % p )

        addProject ( projectConfig )

        heptoolsConfig = self._config.get( 'heptools', None )
        def addHeptools(heptoolsConfig):
            if heptoolsConfig == None:
                return
            heptoolsVersion = heptoolsConfig.get('version')
            heptoolsPackages = heptoolsConfig.get('packages')

            # As is done in the CMake env generation, we add gcc to the LD_LIBRARY_PATH and PATH
            gcc = "gcc"
            gccVersion = getLCGVersion( heptoolsVersion, self._config['cmtconfig'], "gcc" )
            add("env:prepend", "PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/bin".format(h = heptoolsVersion, p = gcc, v = gccVersion))
            add("env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib64".format(h = heptoolsVersion, p = gcc, v = gccVersion))
            add("env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib".format(h = heptoolsVersion, p = gcc, v = gccVersion))
            add("env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib64/python${{PYTHON_VERSION_TWO}}/site-packages".format(h = heptoolsVersion, p = gcc, v = gccVersion))
            add("env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib/python${{PYTHON_VERSION_TWO}}/site-packages".format(h = heptoolsVersion, p = gcc, v = gccVersion))

            # Identify the python version from the packages
            pythonVersion=None
            for pack_info in heptoolsPackages:
                (p, v, t) = pack_info[0:3]
                if p.upper() == "PYTHON":
                    pythonVersion = '.'.join(v.split('.')[:2])

            # Now add entries for the packages
            for pack_info in heptoolsPackages:
                ( p, definev, t ) = pack_info[0:3]
                v = getLCGVersion( heptoolsVersion, self._config['cmtconfig'], p ) or definev
                pkg_path = getLCGPath( heptoolsVersion, self._config['cmtconfig'], p )
                if pkg_path == None:
                  pkg_path = os.path.join( p, v, "$BINARY_TAG" )
                else:
                  if pkg_path.startswith('./'):
                      pkg_path = pkg_path.split( './' )[1]
                if "bin" in t:
                  if p == "gcc":
                    add( "env:prepend", "PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/bin".format( h = heptoolsVersion, p = p, v = v ) )
                  else:
                    add( "env:prepend", "PATH", "${{LCG_releases_base}}/LCG_{h}/{pk}/bin".format( h = heptoolsVersion, pk = pkg_path ) )
                if "lib" in t:
                  if p == "gcc":
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib64".format( h = heptoolsVersion, p = p, v = v ) )
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib".format( h = heptoolsVersion, p = p, v = v ) )
                  else:
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{pk}/lib64".format( h = heptoolsVersion, pk = pkg_path ) )
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_releases_base}}/LCG_{h}/{pk}/lib".format( h = heptoolsVersion, pk = pkg_path ) )
                if "python" in t:
                  if p == "gcc":
                    add( "env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib64/python${{PYTHON_VERSION_TWO}}/site-packages".format( h = heptoolsVersion, p = p, v = v ) )
                    add( "env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{LCG_hostos}}/lib/python${{PYTHON_VERSION_TWO}}/site-packages".format( h = heptoolsVersion, p = p, v = v ) )
                  else:
                    add( "env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{pk}/lib64/python${{PYTHON_VERSION_TWO}}/site-packages".format( h = heptoolsVersion, pk = pkg_path ) )
                    add( "env:prepend", "PYTHONPATH", "${{LCG_releases_base}}/LCG_{h}/{pk}/lib/python${{PYTHON_VERSION_TWO}}/site-packages".format( h = heptoolsVersion, pk = pkg_path ) )
                # Now checking if we have extra variables to set
                # The 4rth argument should be a list of tuples with variables to set
                if len(pack_info) > 3:
                    extra_var_list = pack_info[3]
                    env_home_name = "%s_HOME" % p.upper()
                    env_home_val =  "${{LCG_releases_base}}/LCG_{h}/{p}/{v}/${{BINARY_TAG}}".format( h = heptoolsVersion, p = p, v = v )
                    add( "env:set", env_home_name, env_home_val )
                    for env_var in extra_var_list:
                        add( "env:set", env_var[0], env_var[1] )

#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
#  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>


        addHeptools(heptoolsConfig)

        exttoolsConfig = self._config.get( 'exttools', None )
        def addExttools( exttoolsConfig ):
            if exttoolsConfig == None:
                return
            exttoolsVersion = exttoolsConfig.get( 'version' )
            exttoolsPackages = exttoolsConfig.get( 'packages' )

            # Identify the python version from the packages
            pythonVersion = None
            for pack_info in exttoolsPackages:
                ( p, v, t ) = pack_info[0:3]
                if p.upper() == "PYTHON":
                    pythonVersion = '.'.join( v.split( '.' )[:2] )

            # Now add entries for the packages
            for pack_info in exttoolsPackages:
                ( p, v, t ) = pack_info[0:3]
                if "bin" in t:
                    add( "env:prepend", "PATH", "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}/bin".format( p = p, v = v ) )
                if "lib" in t:
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}/lib64".format( p = p, v = v ) )
                    add( "env:prepend", "LD_LIBRARY_PATH", "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}/lib".format( p = p, v = v ) )
                if "python" in t:
                    add( "env:prepend", "PYTHONPATH", "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}/lib64/python${{PYTHON_VERSION_TWO}}/site-packages".format( p = p, v = v ) )
                    add( "env:prepend", "PYTHONPATH", "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}/lib/python${{PYTHON_VERSION_TWO}}/site-packages".format( p = p, v = v ) )
                # Now checking if we have extra variables to set
                # The 4rth argument should be a list of tuples with variables to set
                if len( pack_info ) > 3:
                    extra_var_list = pack_info[3]
                    env_home_name = "%s_HOME" % p.upper()
                    env_home_val = "${{LCG_external_area}}/Grid/{p}/{v}/${{BINARY_TAG}}".format( p = p, v = v )
                    add( "env:set", env_home_name, env_home_val )
                    for env_var in extra_var_list:
                        add( "env:set", env_var[0], env_var[1] )

#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/bin</env:prepend>
#  <env:prepend variable="PYTHONPATH">${LCG_releases_base}/LCG_83/pytools/1.9_python2.7/${BINARY_TAG}/lib/python2.7/site-packages</env:prepend>
#  <env:prepend variable="PATH">${LCG_releases_base}/LCG_83/Python/2.7.9.p1/${BINARY_TAG}/bin</env:prepend>


        addExttools( exttoolsConfig )


        def addPackage(name):
            add( "env:prepend", "PATH", "${.}/%s/scripts" % name )
            add( "env:prepend", "PYTHONPATH", "${.}/%s/python" % name )
            add( "env:set", "%sROOT" % name.upper(), "${.}/%s" % name )

        if self._config['name'] == 'LbScrips':
            addPackage( "LbConfiguration" )
            addPackage( "LbLegacy" )
            addPackage( "LbNightlyTools" )
            addPackage( "LbRelease" )
            addPackage( "LbScriptsPolicy" )
            addPackage( "LbUtils" )

            add( "env:prepend", "CMAKE_PREFIX_PATH", "${.}/LbRelease/data/DataPkgEnvs:${.}/LbUtils/cmake" )

        add( "env:prepend", "PYTHONPATH", "${.}/lib/python${PYTHON_VERSION_TWO}/site-packages" )
        add( "env:prepend", "PYTHONPATH", "${.}/python${PYTHON_VERSION_TWO}" )
        add( "env:prepend", "PYTHONPATH", "${.}/../.." )
        add( "env:prepend", "LD_LIBRARY_PATH", "${.}/lib" )
        add( "env:prepend", "PATH", "${.}/bin" )
        add( "env:prepend", "PATH", "${.}/../../scripts" )

        return config

if __name__ == "__main__":
    pass
