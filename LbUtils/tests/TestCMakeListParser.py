###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Test of the 

Created on Jun 7, 2016

@author: Ben Couturier
'''
import logging
import os
import sys
import unittest
from os.path import normpath, join

class Test(unittest.TestCase):
    ''' Test case of the AppImporter CMakelists parser '''

    def setUp(self):
        ''' Setup the test '''
        self._data_dir = normpath(join(*([os.path.dirname(__file__)] + ['data'])))
        self._python_dir = normpath(join(*([os.path.dirname(__file__), os.pardir, 'python' ])))
        sys.path.insert(0, self._python_dir)
        logging.basicConfig()
        import LbUtils.Log
        LbUtils.Log._default_log_format = '%(asctime)s:' \
                                          + LbUtils.Log._default_log_format

    def tearDown(self):
        ''' Tear down the test '''
        pass

    def testStandard(self):
        ''' test loading a simple CMakeLists.txt'''
        from LbUtils.CMake import getGaudiUse
        data = '''
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

if(EXISTS ${CMAKE_SOURCE_DIR}/Online/RootCnv)
  set(override_subdirs RootCnv)
endif()

# Declare project name and version
gaudi_project(Test v1r1
              USE Gaudi v27r1
                  Dep1  v2r3
                  Dep2  v4r6
              DATA Gen/DecFiles
                   Det/SQLDDDB VERSION v7r*
                   FieldMap
                   ParamFiles
                   PRConfig
                   RawEventFormat
                   TCK/HltTCK
		   TCK/L0TCK VERSION v5r*)

                   '''

        dependencies = getGaudiUse(data)
        assert(('Gaudi', 'v27r1') in dependencies)
        assert(('Dep1', 'v2r3') in dependencies)
        assert(('Dep2', 'v4r6') in dependencies)
        assert(len(dependencies) == 3)
            
    def testNoData(self):
        ''' test loading a CMakeLists.txt with no DATA section'''
        from LbUtils.CMake import getGaudiUse
        data = '''
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

# Declare project name and version
gaudi_project(Phys v21r4
              USE Rec v19r2)


        '''
        dependencies = getGaudiUse(data)
        assert(('Rec', 'v19r2') in dependencies)
        assert(len(dependencies) == 1)

    def testToolchain(self):
        ''' test loading a CMakeLists.txt with no DATA section'''
        from LbUtils.CMake import getHeptoolsVersion
        data = '''
# taken from Gaudi v27r1
# Special wrapper to load the declared version of the heptools toolchain.
set(heptools_version 84)

if(ENV{HEPTOOLS_VERSION})
  set(heptools_version $ENV{HEPTOOLS_VERSION})
endif()

# this check is needed because the toolchain is called when checking the
# compiler (without the proper cache)
if(NOT CMAKE_SOURCE_DIR MATCHES "CMakeTmp")

 # Note: normally we should look for GaudiDefaultToolchain.cmake, but in Gaudi
 # it is not needed
 include(${CMAKE_SOURCE_DIR}/cmake/GaudiDefaultToolchain.cmake)

endif()
        '''
        htv = getHeptoolsVersion(data)
        assert(htv == "84")


    def testCMakeWithFortran(self):
        ''' test loading a simple CMakeLists.txt'''
        from LbUtils.CMake import getGaudiUse
        data = '''
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

macro(FindG4libs)
    # FIXME: this should be an imported target
    foreach(name ${ARGN})
        #message(STATUS "looking for Geant4 ${name} in ${Geant4_LIBRARY_DIRS}")
        find_library(GEANT4_${name}_LIBRARY
                     G4${name}
                     HINTS ${Geant4_DIR}/lib
                     PATHS ${Geant4_LIBRARY_DIRS})
        #message(STATUS "GEANT4_${name}_LIBRARY -> ${GEANT4_${name}_LIBRARY}")
        list(APPEND GEANT4_LIBS ${GEANT4_${name}_LIBRARY})
    endforeach()
    link_directories(${Geant4_LIBRARY_DIRS})
endmacro()

# Declare project name and version
gaudi_project(Gauss v49r2
              FORTRAN
              USE LHCb v39r1
                  Geant4 v96r4p3
              DATA AppConfig VERSION v3r*
                   BcVegPyData VERSION v2r*
                   Det/GDMLData VERSION v1r*
                   FieldMap VERSION v5r*
                   GenXiccData VERSION v3r*
                   Gen/PGunsData VERSION v1r*
                   Gen/DecFiles VERSION v28r*
                   LHAPDFSets VERSION v2r*
                   MIBData VERSION v3r*
                   ParamFiles VERSION v8r*)


                   '''

        dependencies = getGaudiUse(data)
        assert(('LHCb', 'v39r1') in dependencies)
        assert(('Geant4', 'v96r4p3') in dependencies)
        assert(len(dependencies) == 2)


            
if __name__ == "__main__":
    unittest.main()
